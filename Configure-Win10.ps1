# Remove AppxPackages by creating an array containing PackageFullName and remove using for loop
$packages = @("Microsoft.Wallet", `
              "Microsoft.BingWeather", `
              "Microsoft.MicrosoftStickyNotes", `
              "king.com.FarmHeroesSaga", `
              "king.com.CandyCrushFriends", `
              "Microsoft.WindowsCamera", `
              "Microsoft.XboxIdentityProvider", `
              "Microsoft.Windows.PeopleExperienceHost", `
              "Microsoft.Windows.ParentalControls", `
              "Microsoft.SkypeApp", `
              "Microsoft.ScreenSketch", `
              "Microsoft.ZuneVideo", `
              "Microsoft.ZuneMusic", `
              "Microsoft.YourPhone", `
              "Microsoft.XboxGameCallableUI", `
              "Microsoft.XboxSpeechToTextOverlay", `
              "Microsoft.XboxGamingOverlay", `
              "Microsoft.XboxGameOverlay", `
              "Microsoft.XboxApp", `
              "Microsoft.Xbox.TCUI", `
              "Microsoft.WindowsSoundRecorder", `
              "Microsoft.WindowsMaps", `
              "Microsoft.Print3D", `
              "Microsoft.People", `
              "Microsoft.Windows.Photos", `
              "Microsoft.OneConnect", `
              "Microsoft.Office.OneNote", `
              "Microsoft.MixedReality.Portal", `
              "Microsoft.MicrosoftSolitaireCollection", `
              "Microsoft.MicrosoftOfficeHub", `
              "Microsoft.Microsoft3DViewer", `
              "Microsoft.Messaging", `
              "Microsoft.BingWeather", `
              "Microsoft.Todos", `
              "microsoft.windowscommunicationsapps", `
              "Microsoft.WindowsFeedbackHub", `
              "Microsoft.GetHelp", `
              "Microsoft.MSPaint", `
              "Microsoft.Getstarted", `
              "AppUp.IntelGraphicsExperience", `
              "NVIDIACorp.NVIDIAControlPanel", `
              "SpotifyAB.SpotifyMusic")

Write-Host "Removing Windows Packages..."
foreach ($package in $packages) {
    $found = Get-AppxPackage -AllUsers | where {$_.PackageFullName -Like "$package*"}
    if ($found) {Remove-AppxPackage $found -ErrorAction SilentlyContinue}
}

# Set Control Panel preferences
Write-Host "Making amendments to Explorer..."
Set-Location "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer"
Set-ItemProperty ".\ControlPanel\" AllItemsIconView 1

# Set Explorer preferences
Set-Location "HKCU:\Software\Microsoft\Windows\CurrentVersion"
Set-ItemProperty ".\Explorer\Advanced" ShowSuperHidden 1 # Enables visibility of sensitive OS files
Set-ItemProperty ".\Explorer\Advanced" HideFileExt 0 # Shows file extensions on filenames
Set-ItemProperty ".\Explorer\Advanced" Hidden 1
Set-ItemProperty ".\Explorer\Advanced" LaunchTo 1 # Explorer opens This PC rather than Quick Access
Set-ItemProperty ".\Explorer\Advanced" ShowCortanaButton 0
Set-ItemProperty ".\Search" SearchboxTaskbarMode 0 # Hides the search box in the taskbar

# Install Powershell 7.x to enable Remove-Service command
Invoke-Expression "& { $(Invoke-RestMethod 'https://aka.ms/install-powershell.ps1') } -UseMSI -Quiet"

# Delete telemetry services
Write-Host "Removing bad services..."
$services = @("DiagTrack", "dmwapppushservice", "wersvc", "onesyncsvc", "messagingservice", `
            "wercplsupport", "pcasvc", "wisvc", "retaildemo", "diagsvc", "shpamsvc", `
            "umrdpservice", "SessionEnv", "TroubleshootingSvc")

Set-Location "C:\Program Files\PowerShell\7"
foreach ($service in $services) {
    # Check that the service exists
    if (Get-Service $service -ErrorAction SilentlyContinue) {
        # Get the PID of the service's process
        $servicePID = (Get-WmiObject win32_service | Where-Object {$_.Name -eq $service}).ProcessID
        # Disable the service so it doesn't restart once stopped
        Set-Service $service -StartupType Disabled
        # Kill the process to allow the service to be removed effectively
        Stop-Process $servicePID -Force
        # Remove services
        .\pwsh.exe -Command Remove-Service $service
    }
}

# Enable RDP
Write-Host "Enabling RDP..."
Enable-NetFirewallRule -DisplayGroup "Remote Desktop" # Configure firewall

Set-Location "HKLM:\SYSTEM\CurrentControlSet\Control" # Configure Registry
Set-ItemProperty ".\Terminal Server\" fDenyTSConnections 0 

Set-Location "HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations" # Makes sure NLA is disabled
if ( (Get-ItemProperty ".\RDP-Tcp\" UserAuthentication).UserAuthentication -ne 0 ) {
    Set-ItemProperty ".\RDP-Tcp\" UserAuthentication 0
}

# Set hostname
Write-Host "Setting hostname..."
Rename-Computer -NewName "<host_name>"

# Install scoop then download/install software
Write-Host "Installing scoop..."
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh') # DL & install scoop

$paths = $Env:Path | Out-String # Get PATH

$packages = @('wget', `
              'ripgrep', `
              'firefox', `
              'calibre', `
              'everything', `
              'python', `
              'rufus')

if ($paths -like "*scoop*"){ # Runs only if installed
    Write-Host "Installing software..."
    foreach ($package in $packages){
        scoop install $package
    }
}

# Network config (left until last in case of errors)
if ((Get-NetConnectionProfile -InterfaceAlias "Ethernet").NetworkCategory -ne "Private") {
    Set-NetConnectionProfile -InterfaceAlias Ethernet -NetworkCategory "Private"
}

Write-Host "Setting network config..."
$netAdapterName = "Ethernet"
Disable-NetAdapterBinding -InterfaceAlias $netAdapterName -ComponentID ms_tcpip6

Set-NetIPInterface -InterfaceAlias 'Ethernet' -Dhcp Disabled
New-NetIPAddress -InterfaceAlias "Ethernet" -IPAddress "192.168.1.x" -PrefixLength 24 -DefaultGateway "<gw_ip>"
$index = (Get-NetAdapter -Name "Ethernet").InterfaceIndex
Set-DnsClientServerAddress -InterfaceIndex $index -ServerAddresses ("<pref_dns>", "<pref_dns_2>")

# Restart PC
Restart-Computer -Timeout 3